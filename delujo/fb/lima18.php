<?php  
$json = file_get_contents("https://graph.facebook.com/delujorealestate?fields=albums");
$d = json_decode($json);
foreach ($d->albums->data as $e) {
  if($e->name === $_GET['l']) $albums = file_get_contents("https://graph.facebook.com/{$e->id}/photos"); 
}
$i=1;
$a = json_decode($albums);
?>
<!DOCTYPE html>
<html lang="es">
<head> <title>Delujo.pe</title> <link href='http://fonts.googleapis.com/css?family=Mandali' rel='stylesheet' type='text/css'></head>
<body>
<style type="text/css">
*{margin:0;padding:0;outline:none;}
button{cursor:pointer;}
footer{width: 100%; height: 100px; background: rgba(0, 0, 0, 0.45); position: absolute; bottom: 0px; text-align: center; border-top: 2px solid rgb(255, 255, 255);}
footer>img{padding-top: 40px;}
.cxt{position:relative;}
.main-wrapper{width: 810px; height: 800px; background: url('fondo.jpg'); background-size: 100% 110%; margin: 0 auto;}
.hd-top-header{width: 100%; height: 80px; background: none;}
.hd-top-header>img{width: 140px; padding: 20px;}
.main-slider{width: 100%; height: 320px;}
.main-slider>ul>li{list-style: none; width: 240px; height: 180px; background: rgb(42, 39, 39); float: left; position: absolute; top:140px;box-shadow: 4px 4px 40px black;}
.main-slider>ul>li:nth-child(1){left: 1%; background:none; z-index: 2; height: 130px; top: 190px;}
.main-slider>ul>li:nth-child(2){left: 10%; background: none; z-index: 2;}
.main-slider>ul>li:nth-child(3){left: 28%; background: none; z-index: 2; height: 300px; width: 340px; top: 20px;overflow:hidden;}
.main-slider>ul>li:nth-child(4){right: 10%; background: none; z-index: 1;}
.main-slider>ul>li:nth-child(5){right: 1%; background:none; z-index: 0; height: 130px; top: 190px;}
.main-slider>ul>li>img{width:100%;height:100%;}
.btn-left{width: 50px; height: 50px; background: url('arrow_.png'); background-size: 100%; border: none; position: relative; top: 100px;}
.btn-right{width: 50px; height: 50px; background: url('_arrow.png'); background-size: 100%; border: none; position: absolute; top: 100px; right: 3px;}
.main-description{width: 100%; height: 300px;}
.main-description>ul{width: 640px; height: 130px; margin:30px auto 0; color: white;font-family: 'Mandali', sans-serif; line-height: 1;}
.main-description>ul>li{float: left; width: 300px; padding: 10px;list-style-type: circle;}
.social-actions{width: 350px; margin: 0 auto;}
#conocer_mas{background: url('conocer_mas.png'); width: 150px; height: 50px; background-size: 100% 80%; border: none; background-repeat: no-repeat;}
#contacto{background: url('contactanos.png'); width: 150px; height: 50px; background-size: 100% 80%; border: none; background-repeat: no-repeat;position: absolute; right: 0;}
</style>
<div class="cxt main-wrapper">
  <div class="cxt hd-top-header"> 
      <img src="Lima18logo.png" alt="img_log">
  </div>
  <div class="cxt main-slider">
    <button type="button" class="btn-left"></button>
    <button type="button" class="btn-right"></button>
    <ul>
      <?php 
        foreach ($a->data as $e) {
          $i=$i+1;
          if($i==4) print "<li class='active-image'><img src=".$e->images[0]->source." style='width: 600px; margin-left: -30%;'></li>";
          else print "<li><img src=".$e->images[0]->source."></li>";
          if($i>5) break;
        }
      ?>
    </ul>
  </div>
  <div class="cxt main-description"> 
    <ul>
      <li>Muebles de cocina italianos PEDINI </li>
      <li>Planta de tratamiento de aguas grises, reutilizada para regar jardines e inodoros</li>
      <li>Punto eléctrico y desfogue para deshumedecedores</li>
      <li>Ascensor ecoamigable:  50% menos consumo de energia eléctrica</li>
    </ul>
    <div class="cxt social-actions">
      <button id="conocer_mas" onclick="window.open('http://delujo.pe/index.php/catalogo/19')"></button>
      <button id="contacto"></button>
    </div>
  </div>
  <footer> <img src="http://delujo.pe/assets/images/logo.png"> </footer>
</div>
</body>
</html>