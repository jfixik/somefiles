<?php

class RestaurantController extends BaseController 
{
    public $order_id;

  public function index() {
    $headers = ['Access-Control-Allow-Origin'  => '*'];
    return Response::json( array('success' => 0, 'data' => $this->errs['401']), 401, $headers );
  }
  public function store() {
        Input::merge(array_map('trim', Input::all()));
        $headers = ['Access-Control-Allow-Origin'  => '*'];
        $firstname = Str::upper(substr(e(Input::get('firstname', 'firstname')),0,1));
        $lastname = Str::upper(substr(e(Input::get('lastname', 'lastname')),0,1));
        $custumerid = e(Input::get('custumer_id', null));
        $arr = Input::get('orden',null);
        $email = e(Input::get('email', null));
        $price  = e(Input::get('price', null));
        $people = e(Input::get('people', null));
        $restaurant = e(Input::get('restaurant', null));
        $this->order_id = rand(1000,5000).$firstname[0].$lastname[0];
        $q = DB::table('order_detail')->where('order_id',$this->order_id )->get();
        if(isset($custumerid)  && isset($price)) {
            if($q){ $this->order_id = $firstname[0].$lastname[0].rand(1000,5000); }
            $json = json_decode($arr);
            $result = DB::table('order')->insertGetId( array(
                'people' => $people,
                'total'  => $price,
                'worker_id' => $this->order_id,
                'restaurant_branch_id' => $restaurant
            ));

            foreach($json as $query) {
                $order_d = DB::table('order_detail')->insert(array('order_id' => $result,
                    'custumer_id' => $query->user_id,
                    'quantity' => $query->quantity,
                    'product_id' => $query->product,
                    'price' => $query->price,
                    'prepared' => $query->restaurant,
                    'paid' => false,
                    'delivered' => $query->delivered
                ));
            }
            $data = array('order' => $this->order_id,'usuario' => e(Input::get('firstname', 'firstname')).' '.$lastname,'price' => $price);
            Mail::queue('mail.pedido', $data, function($message) use ($email, $firstname, $lastname) {
                $message->to($email, $firstname)->subject('¿Que se te antoja hoy? |  Valida tu Pedido!!');
            });
            return Response::json( array('success' => 1, 'data' => true,'order_id' => $this->order_id), 200, $headers );
        } else {
            return Response::json( array('success' => 0, 'data' => 'No hay datos'), 200, $headers );
        }
  }
}
