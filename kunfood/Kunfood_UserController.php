<?php

class UserController extends BaseController 
{
  public function index() {
    $headers = ['Access-Control-Allow-Origin'  => '*'];
    return Response::json( array('success' => 0, 'data' => $this->errs['401']), 401, $headers );
  }
  public function show($id) {
    $headers = ['Access-Control-Allow-Origin'  => '*'];
    if(!is_numeric($id))
    {
      return Response::json( array('success' => 0, 'data' => 'Id is not numeric'), 401, $headers );
    } else {
        $result_fav = DB::table('favorite_store')
                ->join('customer','favorite_store.id_costumer', '=', 'customer.id')
                ->join('product','favorite_store.id_product', '=', 'product.id')
                ->select('product.name','product.price','product.time')
                ->where('favorite_store.id_costumer', '=', $id)
                ->get();
        $result_last= DB::table('order')
                ->join('customer','order.worker_id', '=', 'customer.id')
                ->select('people','total','order.created','order.restaurant_branch_id')
                ->where('order.worker_id', '=', $id)
                ->orderby('order.created','desc')
                ->get();
        $camps  = array('id', 'firstname', 'lastname', 'address', 'phone', 'phone02','email','username','id_facebook','points');
            $result = User::find($id, $camps);
        if($result) return Response::json(array('success' => 1, 'customer' => $result->toArray(), 'favorites' => $result_fav, 'last_store' => $result_last), 200, $headers );
        else return Response::json(array('success' => 0, 'customer' => "Not Acceptable"), 200, $headers );
    }
  }
  public function store() {
    $headers = ['Access-Control-Allow-Origin'  => '*', 'Access-Control-Allow-Methods' => 'GET, POST, OPTIONS'];
    $valueInput = Input::get('username');
    if(!empty($valueInput)) {
      $rules = array('username' => 'exists:required|min:5', 'password' => 'exists:required|min:9');
      $result = DB::table('customer')
              ->where('username', (string) e(Input::get('username', null)))
              ->orWhere('password', sha1((string) e(Input::get('password', null))))
              ->take(1)
              ->first();
      if(!empty($result)) {
        return Response::json( array('success' => 1, 'customer' => $result), 200, $headers);
      } else {
        return Response::json( array('success' => 0, 'customer' => $this->errs['400']), 400 , $headers );
      }
    }
    return Response::json( array('success' => 0, 'customer' => $this->errs['400']), 400 , $headers );
  }
}
