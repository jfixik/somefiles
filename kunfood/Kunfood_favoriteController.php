<?php

class favoriteController extends BaseController 
{
  public function index($id) {
    $headers = ['Access-Control-Allow-Origin'  => '*', ];
    if(!is_numeric($id)) {
      return Response::json( array('success' => 0, 'customer' => $this->errs['401']), 401, $headers );
    } else {
      $campos = array('id', 'firstname', 'lastname', 'address', 'phone', 'phone02');
      $result = DB::table('favorite_branch')
                ->join('customer','favorite_branch.customer_id', '=', 'customer.id')
                ->join('restaurant_branch','favorite_branch.branch_id', '=', 'restaurant_branch.id')
                ->select('username','firstname','points','name','latitud','longitud','logo','restaurant_branch.activated')
                ->where('customer.id', '=', $id)
                ->get();
      if($result) return Response::json(array('success' => 1, 'customer' => $result), 200, $headers );
      else return Response::json(array('success' => 0, 'customer' => $this->errs['401']), 401, $headers );
    }
  }
}
