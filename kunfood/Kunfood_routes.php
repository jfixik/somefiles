<?php
Route::pattern('nombre', '[A-Z-a-z]+');
#Route::pattern('lat', '[^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$]+');
#Route::pattern('long','[^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$]+');
Route::group(array("prefix" => "user"), function() {
  Route::get('/', function(){ return Redirect::to('user/actions'); });
    Route::resource('actions', 'UserController');
    Route::resource('actions.favorites', 'favoriteController');
    Route::resource('actions.filter', 'filterController');
    Route::resource('send', 'RestaurantController');
    Route::get('');
    Route::post('confirm/image', function() {
        $headers = ['Access-Control-Allow-Origin'  => '*'];
        if(Request::isMethod('post')) {
            if (Input::file('photo')->isValid()) {
                $destination_path = getcwd().'/image_request/';
                $accept = array('jpg','png');
                $orderID = (int) e(Input::get('order_id', null));
                $UserData = (string) e(Input::get('userdata', null));
                $price = (int) e(Input::get('price', null));
                $email =  e(Input::get('email', null));
                $filename = Input::file('photo')->getClientOriginalName();
                $extension = Input::file('photo')->getClientOriginalExtension();
                if(in_array($extension,$accept)) {
                    $data = array('order' => $orderID,'usuario' => $UserData,'price' => $price);
                    $destination_filename = sha1($filename).rand(100,500).'.'.$extension;
                    $q = DB::table('image_response')->where('src_img',$destination_path.$destination_filename )->get();
                    if($q) { $destination_filename = sha1($filename).rand(100,500).'.'.$extension; }
                    $file = Input::file('photo')->move($destination_path, $destination_filename);
                        $result = DB::table('image_response')->insert( array('user_id' => e(Input::get('user_id', null)),
                            'src_img' => $destination_path.$destination_filename,
                            'order_id' =>e(Input::get('pedido_id', null))
                        ));
                    sendingMailer($data, $email, $UserData);
                } else {
                    return Response::json( array('success' => 0, 'data' => 'fail con el formato'), 401, $headers );
                }
                return Response::json( array('success' => 1, 'data' => $result), 401, $headers );
            }
        } else {
            return Response::json( array('success' => 0, 'data' => 'Fail'), 401, $headers );
        }
    });

    Route::post('register', function() {
          Input::merge(array_map('trim', Input::all()));
          $headers = ['Access-Control-Allow-Origin'  => '*'];
          $last_name = e(Input::get('lastname', null));
          $username  = e(Input::get('username', null));
          $pwd    = sha1(e(Input::get('password', null)));
          $email  = e(Input::get('email', null));
          $name   = e(Input::get('firstname', null));
          $val_db = DB::table('customer')
              ->where('username','=',$username)
              ->where('email','=',$email)
              ->get();
          if(!$val_db) {
      if(!empty($last_name) && !empty($username) && !empty($pwd)) {
                $result = DB::table('customer')->insert( array('lastname' => $last_name,
                    'username' => $username,
                    'password' => $pwd,
                    'email' => $email,
                    'firstname' => $name
                ));
                return Response::json( array('success' => 1, 'data' => $result), 200, $headers );
            } else {
          return Response::json( array('success' => 0, 'data' => 'Fail'), 401, $headers );
            }
          } else {
          return Response::json( array('success' => 0, 'data' => 'El usuario ya existe'), 401, $headers );
          }
        });

  Route::get('actions/filter/{nombre?}/{lat?}/{long?}', function($nombre=null, $lat=null, $long=null) {
      $headers = ['Access-Control-Allow-Origin'  => '*'];
      $result = DB::table('restaurant_branch')
              ->select()
              ->where('name', 'LIKE', (string) $nombre.'%')
              ->orwhere('latitud', 'LIKE', (string) $lat)
              ->orwhere('longitud', 'LIKE', (string) $long)
              ->get();
      return Response::json( array('success' => 1, 'list' => $result), 200, $headers );
    });

    Route::get('validate/{id?}', function($id=null) {
        $result = DB::table('customer')
            ->join('order_detail','customer.id','=','order_detail.custumer_id')
            ->select('customer.id', 'customer.lastname', 'customer.firstname','order_detail.order_id','order_detail.price')
            ->where('order_detail.order_id',$id)
            ->get();
        return View::make('confirm.confirm', array('result' => $result));
    });

    Route::post('actions/save/store', function() {
        $headers = ['Access-Control-Allow-Origin'  => '*'];
        if(Request::isMethod('post')) {
            Input::merge(array_map('trim', Input::all()));
            $userid = e(Input::get('user_id', null));
            $device  = e(Input::get('device', null));
            if(!empty($userid) && !empty($device)) {
                $result = DB::table('customer')->where('id',$userid)
                    ->update( array('deviceid' => $device
                ));
                return Response::json( array('success' => 1, 'data' => $result), 200, $headers );
            } else {
              return Response::json( array('success' => 0, 'data' => 'Fail'), 401, $headers );
            }
        } else {
            return Response::json( array('success' => 0, 'data' => 'Fail'), 401, $headers );
        }
    });

});

Route::group(array("prefix" => "restaurant"), function() {
    Route::get('/', function(){ return Redirect::to('restaurant/actions'); });
        Route::get('locals/{lat?}/{long?}', function($lat=null, $long=null) {
            $headers = ['Access-Control-Allow-Origin'  => '*'];
            $result  = DB::select(DB::raw("SELECT *, ( 6371 * ACOS( COS( RADIANS(:lat) )  * COS(RADIANS( latitud ) ) * COS(RADIANS( longitud ) - RADIANS(:long) )
                    + SIN( RADIANS(".$lat.") ) * SIN(RADIANS( latitud ) ) ) ) AS d_branch FROM restaurant_branch HAVING d_branch < 16.5 ORDER BY d_branch ASC"), array('lat' => $lat, 'long' => $long));
            return Response::json( array('success' => 1, 'list' => $result), 200, $headers );
        });
    Route::get('/actions/{id?}/list', function($id=0) {
      $headers = ['Access-Control-Allow-Origin'  => '*'];
      if(!(int)$id) {
        return Response::json( array('success' => 0, 'customer' => 'Id is not numeric'), 401, $headers );
      }
      $result_list = DB::table('restaurant_branch')
                ->join('product','restaurant_branch.id','=','product.restaurant_branch_id')
          ->select('product.restaurant_branch_id','product.product_category_id','product.name', 'product.price', 'product.time', 'product.image', 'product.description')
          ->where('product.restaurant_branch_id','=',$id)
                ->get();
      return Response::json( array('success' => 1, 'customer' => $result_list), 200, $headers );
    });
});