#!/usr/bin/env perl
#kunfood_demo
use Mojolicious::Lite;
use strict;
use DBI;
use DBD::mysql;
plugin 'PODRenderer';

my $dbh = DBI->connect("dbi:mysql:kunfood_db","root","") or die "ERR___";

helper db => sub { $dbh }; 

get '/' => sub {
  my $c = shift;
  $c->render('index');
};

sub demo {
  any ($a, $b) = @_;
}

options '*' => sub {
  my $self = shift;
  $self->res->headers->header('Access-Control-Allow-Origin'=> '*');
  $self->res->headers->header('Access-Control-Allow-Credentials' => 'true');
  $self->res->headers->header('Access-Control-Allow-Methods' => 'GET, OPTIONS, POST, DELETE, PUT');
  $self->res->headers->header('Access-Control-Allow-Headers' => 'Content-Type, X-CSRF-Token');
  $self->res->headers->header('Access-Control-Max-Age' => '1728000');
  $self->respond_to(any => { data => '', status => 200 });
};

any '/insert/:id' => sub {
  my $self = shift; 
  my $param = $self->stash('id');
  my $q = $dbh->prepare("select * from customer where id='$param'");
  $q->execute;
  my $sql = $q->fetchrow_hashref;
  $q->finish;
  #$self->render('insert');
  return $self->render(json => {user => $sql});
};

get '/(*any)' => sub { 
  shift->render(json => {error => { code => 404, message => 'Not forund' }}) 
};

app->start; 

__DATA__
@@ index.html.ep
% layout 'default';
% title 'Welcome';
% print $q->res->json;

@@ insert.html.ep
% layout 'default';
hola papeto

@@ layouts/default.html.ep
<!DOCTYPE html>
<html>
  <head><title><%= title %></title></head>
  <body><%= content %></body>
</html>