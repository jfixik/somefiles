define(['jquery'], function($)
{
 return {
  filter : function(action, opt) {
 		var actions = ['filter', 'search'];

 		actions.forEach(function(e){
 			if(e==action){
 				filtering();
 			}
 		});

 		function filtering() {
 			$('.'+opt.proyect).css('display','block').siblings().css('display','none');
 			if($('.'+opt.proyect).length<1){
 				$('.mainProject').css('display','block');
 			}
 		}
 	}
 };
});