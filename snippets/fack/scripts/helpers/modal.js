define(['jquery'], function($)
{
 return {
  popup : function(action, opt) {
 		var actions = ['click', 'hover'];

 		actions.forEach(function(e){
 			if(e==action){
 				$('.overlay').fadeIn(opt.transition);
 			}
 		});

 		$('.close').on('click', function(){
 			$('.overlay').fadeOut(opt.transition);
 		});
 	}
 };
});