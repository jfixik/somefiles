require.config({
	baseUrl : 'scripts',
	paths   :  {
		jquery : 'libs/jquery-1.11.1.min'
	}
});

requirejs(['jquery', 'helpers/modal', 'helpers/filternav'], function($,mod,filt)
{

  var i = 0,
      todo = 0,
      ind  = 1,
      scrollArticle = $('.articles_home').offset(),
      slid;

  $('#up_bottom').on('click', function() {
    $('body, html').animate({scrollTop:0}, '700', 'swing', function() { 
     console.log("Top");
    });
  });

  setInterval(function() {
    var lft   = $('#articlesSlice').css('left'),
        slice = lft.split('px');

    slid = slice[0]-1200;

    console.log(ind);
    if(slid === (-3600)){ 
      slid=0;
    }
    if(ind === 3){
      setTimeout(function(){
        ind=0;
      },500);
    }
    $('#articlesSlice').animate({'left':slid+'px'}, 1000, function() {
      ind  = ind+1;
      $('.activeSliceA').next().addClass('activeSliceA');
      $('.slider_articlesIndicator>li:nth-child('+ind+')').addClass('active').siblings().removeClass('active');
    });
  },4000);

  // $('.slider_articlesIndicator').find('li').on('click', function(){
  //   var $this = $(this),
  //       arr_sliced = [0,-1200,-2400,-3600],
  //       $this_data = $this.data('slice');
  //   $this.addClass('active').siblings().removeClass('active');
  //   $('#articlesSlice').animate({'left': arr_sliced[$this_data-1]+'px'}, 1000);
  // });

  function animateStat(e,num) {
     if(todo==0){
        todo=1;
        $('.bottom_stadist_step').animate({'width':'75%'},1000);
        $('.bottom_stadist_one').animate({'width':'16%'},1000);
        $('.bottom_stadist_second').animate({'width':'84%'},1000);
        var aum = setInterval(function() {
         i = i+1;
         $(e).html(i+'%');
         if(i==num){
          clearInterval(aum);
         } 
        },10);
      }
  }
  $('.filterNav').click(function() {
    var $this = $(this),
        dataProyect = $this.data('proyect');
        $this.addClass('active_project').siblings().removeClass('active_project');
        filt.filter('filter',{proyect:dataProyect});
  });

  $('.video_comment').click(function() { 
    mod.popup('click',{transition:500});
  });

  $(document).on('scroll', function() {
    var pos = $(document).scrollTop();
    if(pos >= scrollArticle.top) {
       animateStat('#st1',78);
       animateStat('#st2',16);
       animateStat('#st3',84);
     }
  });
});